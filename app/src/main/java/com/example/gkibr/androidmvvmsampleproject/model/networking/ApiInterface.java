package com.example.gkibr.androidmvvmsampleproject.model.networking;

import com.example.gkibr.androidmvvmsampleproject.model.data.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("discover/movie")
    Call<MovieResponse> getMovies(@Query("api_key") String api_key);
}
