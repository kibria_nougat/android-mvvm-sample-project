package com.example.gkibr.androidmvvmsampleproject.model.networking;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.gkibr.androidmvvmsampleproject.model.data.MovieResponse;
import com.example.gkibr.androidmvvmsampleproject.model.data.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvokeApi {

    private static InvokeApi instance;

    private InvokeApi() {
    }

    public static InvokeApi getInstance() {
        if (instance == null) {
            instance = new InvokeApi();
        }
        return instance;
    }

    public LiveData<List<Result>> getMovieList(String apiKey) {
        final MutableLiveData<List<Result>> data = new MutableLiveData<>();
        ApiClient.getRetrofit().create(ApiInterface.class).getMovies(apiKey).enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                data.setValue(response.body().getResults());
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });

        return data;
    }

}
