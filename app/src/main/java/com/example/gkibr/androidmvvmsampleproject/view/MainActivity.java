package com.example.gkibr.androidmvvmsampleproject.view;


import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.gkibr.androidmvvmsampleproject.R;
import com.example.gkibr.androidmvvmsampleproject.model.data.Result;
import com.example.gkibr.androidmvvmsampleproject.view.adapter.MoviesAdapter;
import com.example.gkibr.androidmvvmsampleproject.view_model.MovieListViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LifecycleOwner {

    @BindView(R.id.rvMovies)
    RecyclerView rvMovies;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final MovieListViewModel viewModel =
                ViewModelProviders.of(this).get(MovieListViewModel.class);

        observeViewModel(viewModel);
    }

    private void observeViewModel(MovieListViewModel viewModel) {
        // Update the list when the data changes
        progressBar.setVisibility(View.VISIBLE);
        viewModel.getMovieListObservable().observe(this, new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> data) {
                if (data != null) {
                    setAdapter(data);
                }
            }
        });
    }

    private void setAdapter(List<Result> data) {
        mLayoutManager = new LinearLayoutManager(this);
        rvMovies.setLayoutManager(mLayoutManager);
        rvMovies.setItemAnimator(new DefaultItemAnimator());
        adapter = new MoviesAdapter(data, MainActivity.this);
        rvMovies.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

}
