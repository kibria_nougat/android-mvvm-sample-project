package com.example.gkibr.androidmvvmsampleproject.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.gkibr.androidmvvmsampleproject.model.AppConstants;
import com.example.gkibr.androidmvvmsampleproject.model.data.Result;
import com.example.gkibr.androidmvvmsampleproject.model.networking.InvokeApi;

import java.util.List;

public class MovieListViewModel extends AndroidViewModel {

    private final LiveData<List<Result>> movieListObservable;

    public MovieListViewModel(Application application) {
        super(application);

        // If any transformation is needed, this can be simply done by Transformations class ...
        movieListObservable = InvokeApi.getInstance().getMovieList(AppConstants.API_KEY);
    }

    /**
     * Expose the LiveData Projects query so the UI can observe it.
     */
    public LiveData<List<Result>> getMovieListObservable() {
        return movieListObservable;
    }

}
